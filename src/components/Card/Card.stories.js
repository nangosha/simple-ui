import CardDemo from './CardDemo';

export default {
  title: 'Components/Card',
  component: CardDemo,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

export const Main = {
  args: {
    title: 'Card Title',
    subtitle: 'Card Subtitle',
  },
};
