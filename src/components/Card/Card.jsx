import React from "react";
import PropTypes from "prop-types";
import "./card.css";

const CardText = React.forwardRef(
  ({ children, className, as: Component = "p", ...props }, ref) => (
    <Component ref={ref} className={`card-text ${className}`} {...props}>
      {children}
    </Component>
  )
);

const CardSubtitle = React.forwardRef(
  ({ children, className, as: Component = "h6", ...props }, ref) => (
    <Component ref={ref} className={`card-subtitle ${className}`} {...props}>
      {children}
    </Component>
  )
);

const CardTitle = React.forwardRef(
  ({ children, className, as: Component = "h5", ...props }, ref) => (
    <Component ref={ref} className={`card-title ${className}`} {...props}>
      {children}
    </Component>
  )
);

const CardBody = React.forwardRef(
  ({ children, className, tag: Tag = "div", ...props }, ref) => (
    <Tag ref={ref} className={`card-body ${className}`} {...props}>
      {children}
    </Tag>
  )
);

const Card = React.forwardRef(
  ({ children, className, as: Component = "div", ...props }, ref) => (
    <Component ref={ref} className={`card ${className}`} {...props}>
      {children}
    </Component>
  )
);

Object.assign(Card, {
  Title: CardTitle,
  Subtitle: CardSubtitle,
  Body: CardBody,
  Text: CardText,
});

export default Card;
